using System.Collections.Generic;
using System.ComponentModel;
using Terraria.ModLoader.Config;

namespace StartWithChests.Common.Configs
{
    public enum VPosStyle {All, Mostly, Half, None};
    public enum HPosStyle {Left, Center, Right};
    public enum ThemeStyle {Basic, Sky, Gold};
    public class StyleConfig : ModConfig
    {
        public override ConfigScope Mode => ConfigScope.ServerSide;
        [DrawTicks]
		[DefaultValue(VPosStyle.Mostly)]
		public VPosStyle Underground;

        [DrawTicks]
		[DefaultValue(HPosStyle.Center)]
		public HPosStyle Position;

        [DefaultValue(true)]
        public bool GenerateHousing;

        [DrawTicks]
		[DefaultValue(ThemeStyle.Sky)]
		public ThemeStyle Theme;
        public List<ItemDefinition> ThemeOverrides;

        public StyleConfig()
        {
            
        }
    }
}
