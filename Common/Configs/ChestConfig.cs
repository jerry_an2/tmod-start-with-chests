using System.Linq;
using System.ComponentModel;
using Terraria.ModLoader.Config;
using System.Collections.Generic;
using Terraria.ID;

namespace StartWithChests.Common.Configs
{
    public class ChestConfig : ModConfig
    {
        public override ConfigScope Mode => ConfigScope.ServerSide;

        [DefaultValue(2)]
        public int nColumns;

        [DefaultValue(6)]
        public int nRows;

        [DefaultValue(4)]
        public int GroupSize;

        [DefaultValue(3)]
        public int Spacing;

        [JsonDefaultListValue("{\"name\": \"Chest\"}")]
        public List<ItemDefinition> UsingChests;

        public ChestConfig()
        {
            UsingChests = new List<ItemDefinition>() {
                new(ItemID.EbonwoodChest),
                new(ItemID.RichMahoganyChest),
                new(ItemID.PearlwoodChest),
                new(ItemID.IvyChest),
                new(ItemID.FrozenChest),
                new(ItemID.SkywareChest),
                new(ItemID.WebCoveredChest),
                new(ItemID.LihzahrdChest),
                new(ItemID.WaterChest),
                new(ItemID.JungleChest),
                new(ItemID.CrimsonChest),
                new(ItemID.CorruptionChest),
                new(ItemID.HallowedChest),
                new(ItemID.IceChest),
                new(ItemID.DesertChest),
                new(ItemID.DynastyChest),
                new(ItemID.HoneyChest),
                new(ItemID.SteampunkChest),
                new(ItemID.PalmWoodChest),
                new(ItemID.MushroomChest),
                new(ItemID.BorealWoodChest),
                new(ItemID.BoneChest),
                new(ItemID.CactusChest),
                new(ItemID.ObsidianChest),
                new(ItemID.PumpkinChest),
                new(ItemID.SpookyChest),
                new(ItemID.GlassChest),
                new(ItemID.MartianChest),
                new(ItemID.MeteoriteChest),
                new(ItemID.GraniteChest),
                new(ItemID.MarbleChest),
                new(ItemID.CrystalChest),
                new(ItemID.GoldenChest),
                new(ItemID.SolarChest),
                new(ItemID.VortexChest),
                new(ItemID.NebulaChest),
                new(ItemID.StardustChest),
                new(ItemID.GolfChest),
                new(ItemID.BambooChest),
                new(ItemID.CoralChest)
            };
        }
    }
}
