using Terraria;
using Terraria.ModLoader;
using Terraria.ID;
using StartWithChests.Common.Configs;
using Terraria.ModLoader.Config;
using System.Collections.Generic;

namespace StartWithChests.Common.Systems
{
	public class StartWithChestsBaseGen : ModSystem
	{
		// Some configurable fields
		private StyleSheet styleSheet;
		private bool housing;
		private int nCols = 4;
        private int nRows = 6;
        private int groupSize = 4;
        private int spacing = 1;
		private List<(ushort type, int style)> chestIDs;
		private int originX;
		private int originY;
		private int mainWidth;
		private int mainHeight;
		private int totalWidth;
		private int totalHeight;

		private bool MyPlaceTile(int i, int j, int type, int style = 0) {
			WorldGen.KillTile(i, j, noItem: true);
			return WorldGen.PlaceTile(i, j, type, true, true, -1, style);
		}

		// Make a rectangle. Optionally add border and wall
		// Go from bottom to top so that most tiles can be properly placed
		private void ClearSpace(int xMin, int yMin, int xMax, int yMax, int borderID = -1, int wallID = -1) {
			for (int i = xMax; i >= xMin; i--) {
				for (int j = yMax; j >= yMin; j--) {
					if (WorldGen.InWorld(i, j)) {
						WorldGen.KillTile(i, j, noItem: true);
						WorldGen.KillWall(i, j);
						if (wallID != -1 && i != xMin && i != xMax && j != yMin && j != yMax)
							WorldGen.PlaceWall(i, j, wallID);
                        if (borderID != -1 && (i == xMin || i == xMax || j == yMin || j == yMax))
							MyPlaceTile(i, j, borderID);
                    }
				}
			}
		}

		private void PlaceNChests(int nChests, int x, int y, int platformID = 0, ushort type = 21, int style = 0) {
			for (int i = 0; i < nChests; i++) {
				MyPlaceTile(x + 2 * i, y, TileID.Platforms, platformID);
                MyPlaceTile(x + 2 * i + 1, y, TileID.Platforms, platformID);
                WorldGen.PlaceChest(x + 2 * i, y - 1, type: type, style: style);
			}
		}

		private void LoadConfig() {
			ChestConfig chestCfg = ModContent.GetInstance<ChestConfig>();
			nCols = chestCfg.nColumns;
			nRows = chestCfg.nRows;
			groupSize = chestCfg.GroupSize;
			spacing = nCols > 0 ? chestCfg.Spacing : 0;
			chestIDs = new List<(ushort type, int style)>();
			foreach (ItemDefinition itemDF in chestCfg.UsingChests) {
				Item item = new(itemDF.Type);
				if (item.createTile > -1 && TileID.Sets.BasicChest[item.createTile]) {
					chestIDs.Add(((ushort)item.createTile, item.placeStyle));
				}
			}
			if (chestIDs.Count == 0) chestIDs.Add((21, 0));

			StyleConfig styleCfg = ModContent.GetInstance<StyleConfig>();
			housing = styleCfg.GenerateHousing;
			mainWidth = groupSize * nCols * 2 + 12 + spacing * nCols - spacing;
            mainHeight = nRows * 3 + 8;
			totalWidth = housing ? mainWidth + 20 : mainWidth;
			totalHeight = mainHeight + 7;

			switch (styleCfg.Underground) {
				case VPosStyle.All: 	originY = Main.spawnTileY + 1; break;
				case VPosStyle.Mostly: 	originY = Main.spawnTileY - 7; break;
				case VPosStyle.Half:	originY = Main.spawnTileY - totalHeight / 2; break;
				case VPosStyle.None: 	originY = Main.spawnTileY - totalHeight; break;
			}
			switch (styleCfg.Position) {
				case HPosStyle.Left:	originX = Main.spawnTileX - totalWidth - 4; break;
				case HPosStyle.Center:	originX = Main.spawnTileX - totalWidth / 2; break;
				case HPosStyle.Right:  	originX = Main.spawnTileX + 4; break;
			}
			styleSheet = new StyleSheet(styleCfg.Theme, styleCfg.ThemeOverrides);
		}

		private void GenerateBase(int x, int y, int nCols, int nRows, int groupSize, int spacing, bool housing = false) {
            // Reserved space on top
            int reservedWidth = totalWidth;
            int reservedHeight = 7;
            ClearSpace(x, y, x + reservedWidth - 1, y + reservedHeight, styleSheet.borderID, styleSheet.wallID);

            int x0 = housing ? x + 10 : x;
			int y0 = y + 7;
            ClearSpace(x0, y0, x0 + mainWidth - 1, y0 + mainHeight - 1, styleSheet.borderID, WallID.Glass);

			// Place light source on top
			int offset = mainWidth / 3 - 1;
            WorldGen.PlaceChand(x0 + offset, y0 + 1, TileID.Chandeliers, styleSheet.chandID);
            WorldGen.PlaceChand(x0 + mainWidth - offset - 1, y0 + 1, TileID.Chandeliers, styleSheet.chandID);
			// Change the blocks between chandeliers to platforms
			for (int a = x0 + offset + 1; a < x0 + mainWidth - offset - 1; a++) {
				MyPlaceTile(a, y0, TileID.Platforms, styleSheet.platformID);
				WorldGen.PlaceWall(a, y0, WallID.Glass);
			}

			// Place chests
            for (int i = 0; i < nCols; i++) {
                for (int j = 0; j < nRows; j++) {
					int chestIdx = (i * nRows + j) % chestIDs.Count;
                    (ushort type, int style) chestID = chestIDs[chestIdx];
                    if (i > 0) for (int k = 0; k < spacing; k++) {
                        MyPlaceTile(x0 + 6 + (2 * groupSize + spacing) * i - k - 1, y0 + 6 + 3 * j, TileID.Platforms, styleSheet.platformID);
                    }
                    PlaceNChests(groupSize, x0 + 6 + (2 * groupSize + spacing) * i, y0 + 6 + 3 * j, styleSheet.platformID, chestID.type, chestID.style);
                }
            }

			int x1 = x;
			int y1 = y + 7;
            int x2 = x0 + mainWidth - 1;
            int y2 = y + 7;
            if (housing) {
				int nFloors = (mainHeight - 1) / 7;
				int houseHeight = nFloors > 0 ? (mainHeight - 1) / nFloors : mainHeight;
				// Make room
				for (int i = 0; i < nFloors; i++) {
					int bottom = i == nFloors - 1 ? y1 + mainHeight - 1 : y1 + (i + 1) * houseHeight;
					// Left
                    ClearSpace(x1, y1 + i * houseHeight, x1 + 10, bottom, styleSheet.borderID, styleSheet.wallID);
					// Right
                    ClearSpace(x2, y2 + i * houseHeight, x2 + 10, bottom, styleSheet.borderID, styleSheet.wallID);
                }
				// Place furnitures
                for (int i = 0; i < nFloors; i++) {
                    int bottom = i == nFloors - 1 ? y1 + mainHeight - 1 : y1 + (i + 1) * houseHeight;
					// Left
                    WorldGen.PlaceDoor(x1, 		bottom - 2, TileID.ClosedDoor, styleSheet.doorID);
                    WorldGen.PlaceDoor(x1 + 10, bottom - 2, TileID.ClosedDoor, styleSheet.doorID);
					WorldGen.PlaceChand(x1 + 5, y1 + i * houseHeight + 1, TileID.Chandeliers, styleSheet.chandID);
					MyPlaceTile(x1 + 5, bottom - 1, styleSheet.tableID.type, styleSheet.tableID.style);
                    WorldGen.PlaceObject(x1 + 3, bottom - 1, TileID.Chairs, true, styleSheet.chairID, direction: 1);
                    WorldGen.PlaceObject(x1 + 7, bottom - 1, TileID.Chairs, true, styleSheet.chairID, direction: -1);
                    // Right
                    WorldGen.PlaceDoor(x2, 		bottom - 2, TileID.ClosedDoor, styleSheet.doorID);
                    WorldGen.PlaceDoor(x2 + 10, bottom - 2, TileID.ClosedDoor, styleSheet.doorID);
                    WorldGen.PlaceChand(x2 + 5, y2 + i * houseHeight + 1, TileID.Chandeliers, styleSheet.chandID);
                    MyPlaceTile(x2 + 5, bottom - 1, styleSheet.tableID.type, styleSheet.tableID.style);
					WorldGen.PlaceObject(x2 + 3, bottom - 1, TileID.Chairs, true, styleSheet.chairID, direction: 1);
                    WorldGen.PlaceObject(x2 + 7, bottom - 1, TileID.Chairs, true, styleSheet.chairID, direction: -1);

					if (i != nFloors - 1) {
						// Place platforms (not forced)
						WorldGen.PlaceTile(x1 - 1,  bottom, TileID.Platforms, mute: true, style: styleSheet.platformID);
						WorldGen.PlaceTile(x1 - 2,  bottom, TileID.Platforms, mute: true, style: styleSheet.platformID);
						WorldGen.PlaceTile(x1 + 11, bottom, TileID.Platforms, mute: true, style: styleSheet.platformID);
						WorldGen.PlaceTile(x1 + 12, bottom, TileID.Platforms, mute: true, style: styleSheet.platformID);
						WorldGen.PlaceTile(x2 - 1,  bottom, TileID.Platforms, mute: true, style: styleSheet.platformID);
						WorldGen.PlaceTile(x2 - 2,  bottom, TileID.Platforms, mute: true, style: styleSheet.platformID);
						WorldGen.PlaceTile(x2 + 11, bottom, TileID.Platforms, mute: true, style: styleSheet.platformID);
						WorldGen.PlaceTile(x2 + 12, bottom, TileID.Platforms, mute: true, style: styleSheet.platformID);
					}
                }
            }
			// Place doors for reserved space
			WorldGen.PlaceDoor(x, 						y + reservedHeight - 2, TileID.ClosedDoor, styleSheet.doorID);
            WorldGen.PlaceDoor(x + reservedWidth - 1, 	y + reservedHeight - 2, TileID.ClosedDoor, styleSheet.doorID);
			WorldGen.PlaceTile(x - 1, 					y + reservedHeight, TileID.Platforms, mute: true, style: styleSheet.platformID);
			WorldGen.PlaceTile(x - 2, 					y + reservedHeight, TileID.Platforms, mute: true, style: styleSheet.platformID);
			WorldGen.PlaceTile(x + reservedWidth, 		y + reservedHeight, TileID.Platforms, mute: true, style: styleSheet.platformID);
			WorldGen.PlaceTile(x + reservedWidth + 1, 	y + reservedHeight, TileID.Platforms, mute: true, style: styleSheet.platformID);
			// Place doors for main room
			WorldGen.PlaceDoor(x0, 					y + totalHeight - 3, TileID.ClosedDoor, styleSheet.doorID);
            WorldGen.PlaceDoor(x0 + mainWidth - 1, 	y + totalHeight - 3, TileID.ClosedDoor, styleSheet.doorID);
        }

        public override void PostWorldGen()
        {
            base.PostWorldGen();
			LoadConfig();
            GenerateBase(originX, originY, nCols, nRows, groupSize, spacing, housing);
        }
    }
}