using System.Collections.Generic;
using StartWithChests.Common.Configs;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader.Config;

namespace StartWithChests.Common.Systems {
    public class StyleSheet
    {
        public int borderID;
		public int wallID;
		public int doorID;
		public int chandID;
		public int platformID;
		public int chairID;
		public (ushort type, int style) tableID;

        public StyleSheet(ThemeStyle style = ThemeStyle.Basic, List<ItemDefinition> ThemeOverrides = null) {
            switch (style) {
				case ThemeStyle.Sky:
					borderID 	= TileID.Sunplate;
					wallID 		= WallID.DiscWall;
					doorID 		= 9;
					chandID 	= 15;
					platformID 	= 22;
					chairID 	= 10;
					tableID 	= (TileID.Tables, 7);
					break;
                case ThemeStyle.Gold:
                    borderID 	= TileID.AncientGoldBrick;
					wallID 		= WallID.AncientGoldBrickWall;
					doorID 		= 21;
					chandID 	= 20;
					platformID 	= 31;
					chairID 	= 19;
					tableID 	= (TileID.Tables, 18);
                    break;
                default:
					borderID 	= TileID.WoodBlock;
					wallID 		= WallID.Wood;
					doorID 		= 0;
					chandID 	= 0;
					platformID 	= 0;
					chairID 	= 0;
					tableID 	= (TileID.Tables, 0);
					break;
			}
			// Check the user-defined override list for any applicable overrides
			if (ThemeOverrides == null) return;
			foreach (ItemDefinition itemDf in ThemeOverrides)
			{
				Item item = new(itemDf.Type);
				if (item.createWall > -1) wallID = item.createWall;
				switch (item.createTile)
				{
					case TileID.ClosedDoor:		doorID = item.placeStyle; 		break;
					case TileID.Chandeliers: 	chandID = item.placeStyle; 		break;
					case TileID.Platforms: 		platformID = item.placeStyle; 	break;
					case TileID.Chairs:			chairID = item.placeStyle; 		break;
					case TileID.Tables: 		tableID = (TileID.Tables, item.placeStyle);		break;
					case TileID.Tables2:		tableID = (TileID.Tables2, item.placeStyle);	break;
					default: if (item.createTile > -1 && Main.tileBrick[item.createTile] &&
								!TileID.Sets.NotReallySolid[item.createTile]) {
						borderID = item.createTile;
					}; break;
				}
			}
        }
    }
}